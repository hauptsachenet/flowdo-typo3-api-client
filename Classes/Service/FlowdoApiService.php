<?php


namespace Hn\FlowdoApiClient\Service;

use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FlowdoApiService implements SingletonInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * At that time it is expected that all cronjobs are done
     */
    protected const CACHE_CLEAR_TIME = '5:00';

    /**
     * @var FrontendInterface
     */
    protected $cache;
    /** @var string */
    protected $baseUrl;
    /** @var string */
    protected $authToken;
    /** @var \DateTimeImmutable */
    protected $clearTimeout;
    /** @var string */
    protected $matomoSiteId;
    /** @var string */
    protected $matomoBaseUrl;

    public function __construct(CacheManager $cacheManager, ExtensionConfiguration $extensionConfiguration)
    {
        $this->clearTimeout = new \DateTimeImmutable(self::CACHE_CLEAR_TIME);
        if ($this->clearTimeout->getTimestamp() < time()) {
            $this->clearTimeout = $this->clearTimeout->modify('+1 day');
        }

        self::modifyPageExpire($this->clearTimeout);

        $this->cache = $cacheManager->getCache('flowdoApiCache');
        $this->baseUrl = $extensionConfiguration->get('flowdo_api_client', 'baseUrl');
        $this->authToken = $extensionConfiguration->get('flowdo_api_client', 'authToken');
        $this->matomoSiteId = $extensionConfiguration->get('flowdo_api_client', 'matomoSiteId');
        $this->matomoBaseUrl = $extensionConfiguration->get('flowdo_api_client', 'matomoBaseUrl');

        if (empty($this->baseUrl)) {
            throw new \InvalidArgumentException('Missing base url');
        }
        if (empty($this->authToken)) {
            throw new \InvalidArgumentException('Missing auth token');
        }
    }

    /**
     * @param \DateTimeImmutable $expires
     */
    private static function modifyPageExpire(\DateTimeImmutable $expires): void
    {
        $newTimeout = $expires->getTimestamp() - time();
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['get_cache_timeout'][] = static function (&$params) use ($newTimeout) {
            return $params['cacheTimeout'] > $newTimeout ? $newTimeout : $params['cacheTimeout'];
        };
    }

    /**
     * @param $path
     * @return bool|array
     */
    public function call($path)
    {

        $cacheKey = sha1($path);
        if (!$this->cache->has($cacheKey)) {
            $this->logger->info("no cache", ['path', $path]);
            try {
                $data = $this->doHttpRequest($path);
            } catch (RequestException $requestException) {
                $this->logger->critical("Exception occurred during request", [
                    'path' => $path,
                    'message', $requestException->getMessage()
                ]);
                return false;
            }

            $lifetime = $this->clearTimeout->getTimestamp() - time();
            if ($lifetime > 0) {
                $this->cache->set($cacheKey, $data, [], $lifetime);
            }
        } else {
            $this->logger->info("from cache", ['path', $path]);
        }

        $result = $this->cache->get($cacheKey);

        if (!$result) {
            return false;
        }
        $result = json_decode($result, true);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $result;
                break;
            case JSON_ERROR_DEPTH:
                throw new \RuntimeException('Maximale Stacktiefe überschritten', JSON_ERROR_DEPTH);
                break;
            case JSON_ERROR_STATE_MISMATCH:
                throw new \RuntimeException('Unterlauf oder Nichtübereinstimmung der Modi', JSON_ERROR_STATE_MISMATCH);
                break;
            case JSON_ERROR_CTRL_CHAR:
                throw new \RuntimeException('Unerwartetes Steuerzeichen gefunden', JSON_ERROR_CTRL_CHAR);
                break;
            case JSON_ERROR_SYNTAX:
                throw new \RuntimeException('Syntaxfehler, ungültiges JSON', JSON_ERROR_SYNTAX);
                break;
            case JSON_ERROR_UTF8:
                throw new \RuntimeException('Missgestaltete UTF-8 Zeichen, möglicherweise fehlerhaft kodiert', JSON_ERROR_UTF8);
                break;
            default:
                throw new \RuntimeException('Unbekannter Fehler');
                break;
        }

    }

    /**
     * @param array|false $results
     * @return array
     */
    protected function parseResults($results): array
    {
        $out = ['total' => 0, 'items' => [], 'first' => '', 'last' => '', 'prev' => '', 'next' => ''];
        if ($results) {

            $out['total'] = $results['hydra:totalItems'];
            $out['items'] = $results['hydra:member'];
            $out['first'] = $results['hydra:view']['hydra:first'];
            $out['last'] = $results['hydra:view']['hydra:last'];
            $out['prev'] = empty($results['hydra:view']['hydra:prev']) ? '' : $results['hydra:view']['hydra:prev'];
            $out['next'] = $results['hydra:view']['hydra:next'];

        }

        return $out;
    }

    /**
     * @param string $term
     * @param int $page
     * @param int $itemsPerPage
     * @param array $additionalParams
     * @param string $queryString
     * @return array
     */
    public function search(string $term, int $page = 1, int $itemsPerPage = 30, array $additionalParams = [], string $queryString = ''): array
    {

        $queryParams = array_merge([
            'page' => $page,
            'itemsPerPage' => $itemsPerPage,
            'querySearch_query' => $term
        ], $additionalParams);

        $path = '/services?' . http_build_query(array_filter($queryParams));

        $path .= $queryString;

        $results = $this->call($path);

        return $this->parseResults($results);

    }

    /**
     * @param string $brand
     * @param array $additionalParams
     * @return array
     */
    public function getCoursesByBrand(string $brand, array $additionalParams = []): array
    {
        $queryParams = array_merge([
            'page' => 1,
            'itemsPerPage' => 99999,
            'brands' => $brand
        ], $additionalParams);

        $path = '/services?' . http_build_query($queryParams);

        $results = $this->call($path);
        return $this->parseResults($results);
    }

    /**
     * @param array $additionalParams
     * @param string $queryString
     * @return array
     */
    public function getAllCourses(array $additionalParams = [], string $queryString = ''): array
    {
        $queryParams = array_merge([
            'page' => 1,
            'itemsPerPage' => 99999,
        ], $additionalParams);

        $path = '/services?' . http_build_query($queryParams);

        $path .= $queryString;

        $results = $this->call($path);
        return $this->parseResults($results);
    }

    /**
     * @param int $id
     * @return bool|mixed
     */
    public function getCourseById(int $id)
    {
        $path = sprintf('/services/%d', $id);
        return $this->call($path);
    }

    /**
     * @param int $id
     * @param array $additionalParams
     * @return array
     */
    public function getCourseDetailsByCourseId(int $id, array $additionalParams = []): array
    {
        $queryParams = array_merge([
            'itemsPerPage' => 99999
        ], $additionalParams);

        $path = sprintf('/services/%d/service_details?%s', $id, http_build_query($queryParams));

        $results = $this->call($path);

        return $this->parseResults($results);
    }

    /**
     * @param array $additionalParams
     * @param string $queryString
     * @return array
     */
    public function getServiceDetails(array $additionalParams = [], string $queryString = ''): array
    {
        $queryParams = array_merge([
        ], $additionalParams);

        $path = sprintf('/service_details?%s', http_build_query($queryParams));

        $path .= $queryString;

        $results = $this->call($path);

        return $this->parseResults($results);
    }

    /**
     * @param int $id
     * @return bool|array
     */
    public function getCourseDetailsById(int $id)
    {
        $path = sprintf('/service_details/%d', $id);

        return $this->call($path);
    }

    /**
     * @param array $additionalParams
     * @param string $queryString
     * @return array
     */
    public function getAllLocations(array $additionalParams = [], string $queryString = ''): array
    {
        $queryParams = array_merge([
            'itemsPerPage' => 99999
        ], $additionalParams);

        $path = sprintf('/locations?%s', http_build_query($queryParams));

        $path .= $queryString;

        $results = $this->call($path);
        return $this->parseResults($results);
    }

    /**
     * @param int $id
     * @return bool|array
     */
    public function getLocationById(int $id)
    {
        $path = sprintf('/locations/%d', $id);

        return $this->call($path);
    }

    /**
     * @param int $count
     * @param array $additionalParams
     * @return array
     */
    public function getRandomCourses(int $count, array $additionalParams = []): array
    {
        $path = '/services?';

        $queryParams = array_merge([
            'page' => rand(1, $this->countPages($path, $count)),
            'itemsPerPage' => $count,
        ], $additionalParams);

        $path .= http_build_query(array_filter($queryParams));

        $results = $this->call($path);

        return $this->parseResults($results);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        $path = sprintf('/categories/');

        $queryParams = array_merge([
            'page' => 1,
            'itemsPerPage' => 99999,
        ]);

        $path .= '?' . http_build_query($queryParams);

        $results = $this->call($path);

        return $this->parseResults($results);
    }

    /**
     * @param int $id
     * @return bool|array
     */
    public function getCategory(int $id)
    {
        $path = sprintf('/categories/%d', $id);

        return $this->call($path);
    }

    /**
     * @param string $path
     * @param int $itemsPerPage
     * @return false|float
     */
    private function countPages(string $path, int $itemsPerPage)
    {
        $results = $this->call($path);
        $results = $this->parseResults($results);

        return floor($results['total'] / $itemsPerPage);
    }

    /**
     * @param array $additionalParams
     * @param string $queryString
     * @return array
     */
    public function getZipcodes(array $additionalParams = [], string $queryString = ''): array
    {
        $path = sprintf('/zipcodes?%s', http_build_query($additionalParams));

        $path .= $queryString;

        $results = $this->call($path);
        return $this->parseResults($results);
    }

    protected function sendMatomoStats(string $path, int $requestTime)
    {
        if (empty($this->matomoBaseUrl) || empty($this->matomoSiteId)) {
            return;
        }

        $apiEndpoint = explode('/', trim($path, '/'))[0];
        $apiUrl = $this->baseUrl . $path;

        // https://developer.matomo.org/api-reference/tracking-api
        $request = [
            'idsite' => $this->matomoSiteId,
            'rec' => 1,
            'action_name' => sprintf('api/%s', $apiEndpoint),
            'url' => $apiUrl,
            '_id' => 'flowdoapixxxxxxx',
            'rand' => GeneralUtility::milliseconds(),
            'apiv' => 1,
            'h' => date('H'),
            'm' => date('i'),
            's' => date('s'),
            'ua' => 'flowdoApiClient/v1',
            'uid' => 'flowdoApiClient',
            'gt_ms' => $requestTime,
            'pf_srv' => $requestTime,
            'send_image' => 0,
        ];

        GeneralUtility::getUrl($this->matomoBaseUrl . '?' . http_build_query($request), 0);
    }

    /**
     * @param $path
     * @return mixed
     * @throws RequestException
     */
    protected function doHttpRequest($path)
    {
        $requestStart = GeneralUtility::milliseconds();
        $report = [];
        $requestHeaders = [
            'X-AUTH-TOKEN' => $this->authToken
        ];
        $data = GeneralUtility::getUrl(
            $this->baseUrl . $path,
            0,
            $requestHeaders,
            $report
        );

        if ($data === false && $report['exception']) {
            throw $report['exception'];
        }

        $requestEnd = GeneralUtility::milliseconds();
        $this->sendMatomoStats($path, $requestEnd - $requestStart);
        $this->logger->info("request finished", ['result' => $data, 'duration' => $requestEnd - $requestStart]);
        return $data;
    }

}
