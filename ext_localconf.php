<?php
// Create API cache
// Caching framework
if( !is_array($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations']['flowdoApiCache'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['flowdoApiCache'] = [];
}
// Hier ist der entscheidende Punkt! Es ist der Cache von Variablen gesetzt!
if( !isset($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations']['flowdoApiCache']['frontend'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['flowdoApiCache']['frontend'] = \TYPO3\CMS\Core\Cache\Frontend\VariableFrontend::class;
}
if( !isset($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations']['flowdoApiCache']['backend'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['flowdoApiCache']['backend'] = \TYPO3\CMS\Core\Cache\Backend\FileBackend::class;
}
// Wie lange soll der Cache haltbar sein? (1 Stunde)
if( !isset($GLOBALS['TYPO3_CONF_VARS'] ['SYS']['caching']['cacheConfigurations']['flowdoApiCache']['options'] ) ) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['flowdoApiCache']['options'] = ['defaultLifetime' => 3600];
}

$GLOBALS['TYPO3_CONF_VARS']['LOG']['Hn']['FlowdoApiClient']['writerConfiguration'] = [
    \TYPO3\CMS\Core\Log\LogLevel::DEBUG => [
        \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/flowdo_api_client.log'
        ]
    ],
];
