<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'hauptsache.net Flowdo TYPO3 API Client',
    'description' => '',
    'category' => 'misc',
    'state' => 'excludeFromUpdates',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'constraints' => [
        'depends' => [],
        'conflicts' => [],
        'suggests' => [],
    ],
    'author' => '',
    'author_email' => '',
    'author_company' => '',
    'version' => '1.0.4'
];
