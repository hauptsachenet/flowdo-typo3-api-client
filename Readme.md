# TYPO3 Extension "Flowdo TYPO3 API Client"
This extension provides a service class to wrap the Flowdo API calls.

It also modifies the systems cache timeout to prevent stale pages.

## Installation
`composer require hn/flowdo-api-client`

and activate the extension in the extension manager.

## Setup
You need to provide some values for the TYPO3 constants defined in `ext_conf_template.txt`.

`baseUrl` needs to point to the API entrypoint

`authToken` is you API auth token

## Usage
Inject the `FlowdoApiService` into your controller and call its methods to fetch course informations from the Flowdo API.

(See [DependencyInjection](https://wiki.typo3.org/Dependency_Injection) in the TYPO3 Wiki.)
